import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Select_Index {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Haran\\eclipse-workspace\\Automation_03\\driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get("https://demoqa.com/select-menu");
		Thread.sleep(5000);
		Select dd = new Select(driver.findElementByXPath("//select[@id='oldSelectMenu']"));
		//dd.selectByIndex(0);
		//driver.findElementByXPath("//select[@id='oldSelectMenu']").getText();
		
		  dd.selectByValue("7");
		  dd.getOptions();
		  //System.out.println(driver.findElementByXPath("//select[@id='oldSelectMenu']").getText());
		  //dd.selectByVisibleText("Black");
		  //driver.findElementByXPath("//select[@id='oldSelectMenu']").getText();
		  System.out.println("Selected option:"+dd.getFirstSelectedOption().getText());
		  List<WebElement> options = dd.getOptions();
		  for (WebElement list : options) {
			  System.out.println(list.getText());
			
		}
		  boolean dropstatus=dd.isMultiple();
		  System.out.println(dropstatus);
		 
	}

}
