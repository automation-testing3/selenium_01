package assignment_01;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class EaseMyTrip {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\Haran\\eclipse-workspace\\Automation_03\\driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get("https://www.easemytrip.com/");
		Thread.sleep(5000);
		System.out.println(driver.findElementByXPath("(//a[@class='hm_link'])[10]").getText());
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='srch_btn'])[10]").click();
		Thread.sleep(5000);
		String PresentPageTitle = driver.getTitle();
		System.out.println(PresentPageTitle);
		String ActualPageTitle="FlightList  Lowest Airfare, Flight Tickets, Cheap Air Tickets � EaseMyTrip.com";
		if (PresentPageTitle.equals(ActualPageTitle)) {
			System.out.println("Page title confirmed");
			System.out.println(PresentPageTitle);
		} else {
			System.out.println("Page title not confirmed");
		}

		Thread.sleep(5000);
		String GivenPrice = "6,159";
		String PriceXpath = "(//div[contains(text(),'" + GivenPrice + "')])[1]";
		System.out.println(PriceXpath);

		String SearchedPrice = driver.findElementByXPath(PriceXpath).getText();
		System.out.println("Searched price:" + SearchedPrice);

		if (SearchedPrice.equals(GivenPrice)) {
			String ButtonXpath = "("+PriceXpath+"//..//..//..//div)[21]//button";
			System.out.println(ButtonXpath);
			driver.findElementByXPath(ButtonXpath).click();
		} else {
			System.out.println("Given price is not listed in current page");
		}
		Thread.sleep(5000);
		driver.navigate().back();
		Thread.sleep(5000);
		driver.navigate().forward();
		Thread.sleep(5000);
		System.out.println(driver.findElementByXPath("(//div[@class='pr'])[2]").getText());
		Thread.sleep(5000);
		//driver.quit();

	}

}
